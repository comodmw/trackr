module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true
  },
  extends: 'standard',
  globals: {
    __static: true
  },
  plugins: [
    'html'
  ],
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    "no-mixed-spaces-and-tabs": [0],
    "no-tabs": 0,
    "skipBlankLines": 0,
    "ignoreComments": 0,
    "no-trailing-spaces": [2, { "skipBlankLines": true }],
    "semi": 0,
    "comma-dangle": 0,
    "eol-last": 0,
    "padded-blocks": 0,
    "handle-callback-err": 0,
    "space-before-function-paren": 0,
    "no-unused-vars": 0,
    "no-multiple-empty-lines": 0,
    "space-in-parens": 0,
    "indent": 0,
    "keyword-spacing": 0,
  }
}
