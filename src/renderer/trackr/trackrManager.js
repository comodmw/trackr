export default class TrackrManager {

    trackrRegister = []

    groupIdCnt = 0

    /**
     * Register a trackrRegister
     *
     * @param trackr
     */
    register(trackr) {
        this.trackrRegister.push(trackr)
        trackr.setGroupId(this.groupIdCnt++)
    }

    /**
     * Init all trackr
     */
    init() {
        this.trackrRegister.forEach(function (trackr) {
            trackr.init()
        })
    }

    render() {

        let allRanges = []
        this.trackrRegister.forEach(function (trackr) {

            let trackrActivityItems = trackr.getActivityItems()

            if (typeof trackrActivityItems === 'undefined') {
                return
            }

            let ranges = trackr.render()
            if (typeof ranges === 'undefined') {
                return
            }
            allRanges = allRanges.concat(ranges)

        })

        let i = 1;

        return allRanges.map(function (e) {
            return {
                ...e,
                id: i++
            }
        });

    }

}