import TrackrAbstract from './trackrAbstract.js'
const monitor = require('active-window');
const moment = require('moment');
export default class TrackrActiveWindow extends TrackrAbstract {

    name = 'activeWindow';
    label = 'Active Window'

    init() {

        let that = this

        let currentWindow
        let callback = function(window) {
            try {

                let windowName = window.app + window.title
                if (currentWindow === windowName) {
                    return
                }
                currentWindow = windowName
                // console.log(window.app);
                // console.log(window.title);

                // Save
                let activityItem = that.createActivityItem({
                    app: window.app,
                    title: window.title
                });

                let context = that.store
                context.commit('addActivityItem', activityItem)

            }catch(err) {
                // console.log(err);
            }
        }

        // Get the current active window every second
        monitor.getActiveWindow(callback, -1, 1);
    }

    render() {
        let activityItems = this.getActivityItems()
        // let name = this.getName()
        let groupId = this.getGroupId()
        return activityItems.map(function (e) {
            let payload = e.label
            return {
                group: groupId,
                content: '',
                title: payload.title + '' + payload.app,
                start: e.start,
                end: null, // e.end,
                type: 'point' // (e.end === null) ? 'point' : null,
            }
        });
    }

}