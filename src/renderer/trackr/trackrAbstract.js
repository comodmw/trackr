const moment = require('moment');
export default class TrackrAbstract {

    store
    name = 'abstract'

    constructor(store) {
        this.store = store
    }

    getName() {
        return this.name
    }

    setGroupId(groupId) {
        this.groupId = groupId
    }

    getGroupId() {
        return this.groupId
    }

    init() {
        // ...
    }

    render() {
        // ...
    }

    getActivityItems() {
        let activityItems = this.store.state.data._activityItems
        return activityItems[this.getName()]
    }

    createActivityItem(label, start = null, end = null) {
        return {
            id: this.name,
            start: (start === null) ? moment() : start,
            end,
            label // rename to payload
        }
    }

    addActivityItem() {
        // context.commit('addActivityItem', activityItem)
    }

    getActivityItem() {
        // ...
    }

    getOptions() {
        // ...
    }


}