import TrackrAbstract from './trackrAbstract.js'
import store from '../store';
// const monitor = require('active-window');
const moment = require('moment');
const ioHook = require('iohook')
export default class trackrActivity extends TrackrAbstract {

    name = 'activity'
    label = 'Activity'

    threshHoldSeconds = 5; // @todo should be a user setting

    /**
     * Init trackrRegister - "Activity" (IOHook - Tracks I/O-Activity (Mouse/Keyboard))
     */
    init() {

        let that = this

        function ioHookEventHandler(event) {

            // Save
            let activityItem = that.createActivityItem(event);

            let context = that.store
            context.commit('addActivityItem', activityItem)

        }

        ioHook.start(false)

        // @todo should be a user setting
        // ioHook.on('mousewheel', ioHookEventHandler)
        // ioHook.on('mousemove', ioHookEventHandler)
        // ioHook.on('mouseclick', ioHookEventHandler)
        ioHook.on('keyup', ioHookEventHandler)
        // ioHook.on('keydown', ioHookEventHandler)

    }

    render() {

        let threshHoldSeconds = this.threshHoldSeconds

        // let now = moment()

        let activityItems = this.getActivityItems()

        // let lastActivityItem
        let currentActivityRange = {
            start: null,
            end: null
        }

        let ranges = []

        let collectNew = true

        activityItems.forEach(function(e) {

            if (collectNew) {
                collectNew = false
                currentActivityRange.start = moment(e.start)
                currentActivityRange.end = moment(e.start)
                return
            }

            let currentEnd = moment(currentActivityRange.end)
            let thisStart = moment(e.start)

            let diffSeconds = thisStart.diff(currentEnd, 'seconds')

            if (diffSeconds < threshHoldSeconds) {
              // append
                currentActivityRange.end = thisStart
            } else {
              // cut
                ranges.push({...currentActivityRange})
                collectNew = true
            }

        })

        ranges.push({...currentActivityRange})

        // context.commit('data', {key: 'currentActivityRange', data: currentActivityRange})

        // let activityItems = this.getActivityItems()
        // // let name = this.getName()

        let groupId = this.getGroupId()
        let result = ranges.map(function (e) {
            return {
                group: groupId,
                content: '',
                title: '',
                start: e.start,
                end: e.end,
                type: (e.end === null || e.start === e.end) ? 'point' : null,
            }
        });

        console.log(result);

        return result
    }

}