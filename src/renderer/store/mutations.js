export default {
  data(state, payload) {
    // console.log('save data', payload.key, payload.data);
    state.data[payload.key] = payload.data;
  },
  // activityItems(state, data) {
  //   state.data.activityItems = data;
  // },
  addActivityItem(state, data) {
    state.data.activityItems.push(data);

    if (!state.data._activityItems.hasOwnProperty(data.id)) {
      state.data._activityItems[data.id] = []
    }

    state.data._activityItems[data.id].push(data);

  },
  registerGitWorkingPath(state, data) {
    // console.log('save data', payload.key, payload.data);
    state.data.gitWorkingPaths.push(data)
  },
  deleteGitWorkingPath(state, data) {
    let i = state.data.gitWorkingPaths.indexOf(data);
    if (i !== -1) {
      state.data.gitWorkingPaths.splice(i, 1);
    }
  },
  gitCurrentBranch(state, payload) {
    // console.log('save data', payload.key, payload.data);
    state.data.gitCurrentBranch[payload.key] = payload.data;
  },
}