import {trackrManager} from '../main'
const moment = require('moment');

// @todo move to config
let trackr = {
  activity: {
    groupId: 0,
    label: 'Activity'
  },
  checkout: {
    groupId: 1,
    label: 'Checkout'
  },
  activeWindow: {
    groupId: 2,
    label: 'Active Window'
  },
};

function getLabel(e) {
  if (e.label === null) return ''
  if (typeof e.label === 'string') return e.label

  if (typeof e.label !== 'object') return ''

  let payload = e.label

  switch (e.id) {
    case 'activeWindow':
      return ''
    case 'activity':
      return ''
  }

}
function getTooltip(e) {
  if (e.label === null) return ''
  if (typeof e.label === 'string') return e.label

  if (typeof e.label !== 'object') return ''

  let payload = e.label

  switch (e.id) {
    case 'activeWindow':
      return payload.title + '' + payload.app
  }

}

export default {
  data (state) {
    return state.data;
  },
  timeline (state) {

    let currentActivityRange = state.data.currentActivityRange

    // Fit Timeline
    // let start = moment().startOf('hour');
    let start = moment().subtract(20, 'minutes')
    let end = moment().endOf('hour');
    if (typeof currentActivityRange !== 'undefined') {
      end = moment(currentActivityRange.end).add(2, 'minutes')
    }
    // let start = null
    // let end = null

    let groups = Object.keys(trackr).map(function(key) {
      let value = trackr[key];
      return {
        id: value.groupId,
        content: value.label
      }
    });

    // @todo get groups from trackrManager
    // console.log(groups);

    return {

      groups,
      options: {
        start,
        end,
        editable: true,
      }
    }

  },
  ranges (state) {

    let allRanges = trackrManager.render()
    // console.log(allRanges);
    return allRanges

    // // @todo refactor the code below to trackrManager.render()
    //
    // let currentActivityRange = state.data.currentActivityRange // because of that live handling
    //
    // // @todo check for any tracker
    // if (typeof currentActivityRange.end === 'undefined') {
    //   return [{
    //     id: 1,
    //     group: 'activity',
    //     content: 'Welcome',
    //     start: moment(),
    //   }];
    //
    // }
    //
    // let ranges = state.data.activityItems
    // let preparedRanges = ranges.concat([currentActivityRange])
    //
    // let id = 1;
    //
    // let preparedRangesFinal = preparedRanges.map(function (e) {
    //   return {
    //     id: id++,
    //     group: trackr[e.id].groupId,
    //     content: getLabel(e),
    //     title: getTooltip(e),
    //     start: e.start,
    //     end: e.end,
    //     type: (e.end === null) ? 'point' : null,
    //   }
    // });
    //
    // console.log(preparedRangesFinal);
    //
    // return preparedRangesFinal

  }
}