export default {
  data: {
    activityItems: [], // @todo: make obsolete
    _activityItems: {}, // actually activityItems but with trackr index
    currentActivityRange: {},
    gitWorkingPaths: [],
    gitCurrentBranch: [],
  }
}