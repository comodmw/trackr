import {trackrManager} from '../main'
import store from '../store'

const moment = require('moment');
const fs = require('fs'); // Load the File System to execute our common tasks (CRUD)
// const path = require('path');
const filewatcher = require('filewatcher');
const watcher = filewatcher();
const simpleGit = require('simple-git');



const trackSeconds = 60 * 5; // @todo add to config

const gitDir = '/.git'
const ideaDir = '/.idea'
// const vendorDir = '/vendor'
//
// let gitWorkingPaths;
let currentBranch = []
let git = [];
let lockWatcher = false;

function createActivityItem(id, start, end, label) {
  return {
    id,
    start: moment(),
    end,
    label // string or payload
  }
}



function addWatcher(path) {
  watcher.add(path + ideaDir)
  watcher.add(path + gitDir)
  git[path] = simpleGit(path);
}

function deleteWatcher(path) {
  watcher.remove(path + ideaDir)
  watcher.remove(path + gitDir)
  git[path] = null;
}

export default {
  onInit(context) {

    /**
     * Init trackrRegister "Checkout"
     */
    // init FileWatcher for .idea and .git activities
    let fileWatcherOnChange = function(file, stat) {
      // let timestamp = moment(stats.mtime);
      // @todo use constants for tracker
      store.dispatch('checkGitStatus')
      // store.dispatch('track', {id: 'code', payload: {file, timestamp}});
      // if (!stat) console.log('deleted');
    };
    watcher.on('change', fileWatcherOnChange);

    let gitWorkingPaths = store.getters.data.gitWorkingPaths
    gitWorkingPaths.forEach(function(path) {
      addWatcher(path);
    })

    trackrManager.init()

  },
  data({commit}, payload) {
    commit('data', payload);
  },
  registerGitWorkingPath({commit}, path) {
    addWatcher(path);
    commit('registerGitWorkingPath', path);
  },
  deleteGitWorkingPath({commit}, path) {
    deleteWatcher(path);
    commit('deleteGitWorkingPath', path);
  },
  checkGitStatus({commit}, payload) {

    if (lockWatcher) {
      return;
    }
    lockWatcher = true;

    let gitWorkingPaths = store.getters.data.gitWorkingPaths
    gitWorkingPaths.forEach(function (path) {

      let gitProject = git[path];
      if (!gitProject) {
        return;
      }

      gitProject.status(function (err, status) {

        /**
         * avoid ugly endless loop
         *
         * it seems that gitProject.status produces
         * another .git fileWatching event
         */
        setTimeout(function() {
          lockWatcher = false;
        }, 100);

        let branch = status.current;

        if (branch !== null && branch !== currentBranch[path]) {

          if (currentBranch[path] !== undefined) {
            store.dispatch('track', {id: 'checkout', payload: {branch, path}});
          }

          currentBranch[path] = branch;
          // let name = path.substring(path.lastIndexOf('/') + 1, path.length);
          // console.log('Current Branch for ' + name + ' is: ' + thisBranch);
          // console.log(currentBranch);

        }
      });
    });
  },

  /*
   * @todo at the end this method is useless and was replaced by getter
   */
  track(context, payload) {

    let trackerId = payload.id
    let trackerPayload = payload.payload

    switch(trackerId) {

      // // @todo use constants
      // case 'activity':
      //
      //   let now = trackerPayload
      //
      //   let currentActivityRange = { ...context.getters.data.currentActivityRange };
      //
      //   let then = moment(currentActivityRange.end)
      //
      //   if (Object.keys(currentActivityRange).length === 0) {
      //     currentActivityRange = createActivityItem('activity', now, now)
      //   }
      //
      //   if (typeof then === 'object') {
      //
      //     let diffSeconds = now.diff(then, 'seconds')
      //
      //     if (diffSeconds < trackSeconds) {
      //       // append
      //       currentActivityRange.end = now
      //     } else {
      //       // cut - commit currentRange and start a new one
      //       context.commit('addActivityItem', currentActivityRange)
      //       currentActivityRange = createActivityItem('activity', now, now)
      //     }
      //   }
      //
      //   context.commit('data', {key: 'currentActivityRange', data: currentActivityRange})
      //
      //   break;

      case 'checkout':

        let path = trackerPayload['path'];
        let branch = trackerPayload['branch'];
        let checkoutName = path.substring(path.lastIndexOf('/') + 1, path.length);

        let checkoutItem = createActivityItem('checkout', moment(), null, checkoutName + ': ' + branch);
        context.commit('addActivityItem', checkoutItem)
        break;

      case 'code':

        let timestamp = trackerPayload['timestamp']
        let file = trackerPayload['file']
        // let name = file.substring(file.lastIndexOf('/') + 1, file.length);
        // @todo save whole payload
        let codeItem = createActivityItem('code', timestamp, null, file);
        context.commit('addActivityItem', codeItem)
        break;

    }

  },
}