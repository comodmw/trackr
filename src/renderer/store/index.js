import Vue from 'vue'
import Vuex from 'vuex'
import state from './state';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';
import modules from './modules'

import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  // storage: {}
  storage: window.localStorage
  // storage: MyStorage
});

export default new Vuex.Store({
  // modules,
  state,
  getters,
  actions, // Asynchronously (dispatch)
  mutations, // Synchronously (commit)
  strict: process.env.NODE_ENV !== 'production',
  plugins: [vuexLocal.plugin]
})