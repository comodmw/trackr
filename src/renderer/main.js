import Vue from 'vue'
import axios from 'axios'
import App from './App'
import router from './router'
import store from './store'
import {Timeline} from 'vue2vis';

import TrackrManager from './trackr/trackrManager'
import TrackrActivity from './trackr/trackrActivity'
import TrackrCheckout from './trackr/trackrCheckout'
import TrackrActiveWindow from './trackr/trackrActiveWindow'

let moment = require('moment');
export default moment

Vue.component('timeline', Timeline);

// Register all trackrRegister
let trackrManager = new TrackrManager()
let trackrRegister = [
  TrackrActivity,
  TrackrCheckout,
  TrackrActiveWindow
]
trackrRegister.forEach(function(Trackr) {
  trackrManager.register(new Trackr(store))
})

export {trackrManager}
// modules.export = {trackrManager}

// Init
store.dispatch('onInit');

// /**
//  * Auto Setup - Find idea/git directories automatically @todo not as easy as expected
//  */
// let _path = '/mnt/data/Bibliotheken/Development/workspace/check24'
// let _path = '/mnt/data/Bibliotheken/Development/workspace/test'
// let _path = '/mnt/data/Bibliotheken/Development/workspace/check24/tier'
// let gitDir = '.git'
// let ideaDir = '.idea'
// let vendorDir = 'vendor'
//
// let walk = function(dir, done) {
//   let results = [];
//   fs.readdir(dir, function(err, list) {
//     if (err) return done(err);
//     let pending = list.length;
//     if (!pending) return done(null, results);
//     list.forEach(function(file) {
//       file = path.resolve(dir, file);
//       fs.stat(file, function(err, stat) {
//         if (stat && stat.isDirectory()) {
//           // results.push(file);
//           let name = file.substring(file.lastIndexOf('/') + 1, file.length);
//
//           if (name === vendorDir) if (!--pending) done(null, results);
//
//           if (name === gitDir) {
//             results.push(file);
//           }
//
//           walk(file, function(err, res) {
//             results = results.concat(res);
//             if (!--pending) done(null, results);
//           });
//
//         } else {
//           // results.push(file);
//           if (!--pending) done(null, results);
//         }
//       });
//     });
//   });
// };

/**
 * Vue
 */
if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: {App},
  router,
  store,
  template: '<App/>'
}).$mount('#app')
