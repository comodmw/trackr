# trackr

Automated time tracking tool

## Install & Develop

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev
```

## Dependencies

You may need to install some dependencies manually.
(Under Linux this works out of the box!)

### Install electron-vue
https://github.com/SimulatedGREG/electron-vue

### Install python
https://www.python.org/downloads/release/python-278/

### Install more deps
https://www.microsoft.com/en-us/download/confirmation.aspx?id=40760
npm install --global --production windows-build-tools
npm install -g node-gyp
npm install -g --msvs_version=2013 node-gyp rebuild
npm -g install npm@next

### Install IOHook
https://github.com/WilixLead/iohook
npm install iohook --save

## Build & More

``` bash
# build electron application for production
npm run build

# lint all JS/Vue component files in `src/`
npm run lint

```
---